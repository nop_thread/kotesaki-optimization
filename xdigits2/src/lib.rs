#![feature(test)]
extern crate test;

pub fn decode1([upper, lower]: [u8; 2]) -> u8 {
    lower - b'0' - ((lower >> 4) ^ 3) + (upper << 4).wrapping_sub((upper & 0xf0) ^ 0x30)
}

pub fn decode2([upper, lower]: [u8; 2]) -> u8 {
    let i_upper = match upper {
        b'0'..=b'9' => upper - b'0',
        _ => upper - b'A',
    };
    let i_lower = match lower {
        b'0'..=b'9' => lower - b'0',
        _ => lower - b'A',
    };
    (i_upper << 4) + i_lower
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    fn create_input() -> [[u8; 2]; 256] {
        const XDIGITS: [u8; 16] = *b"0123456789ABCDEF";
        let mut arr = [[0_u8; 2]; 256];
        XDIGITS
            .into_iter()
            .flat_map(|u| XDIGITS.into_iter().map(move |l| (u, l)))
            .zip(&mut arr)
            .for_each(|((u, l), a)| *a = [u, l]);
        arr
    }

    #[bench]
    fn micro_optimized(b: &mut Bencher) {
        let inputs = &create_input();
        let n = test::black_box(10000);
        b.iter(|| {
            (0..n)
                .map(|_| inputs.iter().copied().map(decode1).fold(0, |a, b| a ^ b))
                .fold(0, |a, b| a ^ b)
        })
    }

    #[bench]
    fn simple(b: &mut Bencher) {
        let inputs = &create_input();
        let n = test::black_box(10000);
        b.iter(|| {
            (0..n)
                .map(|_| inputs.iter().copied().map(decode2).fold(0, |a, b| a ^ b))
                .fold(0, |a, b| a ^ b)
        })
    }
}
