## Result

I won.

```
$ inxi
CPU: 8-core AMD Ryzen 7 3800X (-MT MCP-) speed/min/max: 2209/2200/3900 MHz
Kernel: 5.17.3-gentoo x86_64 Up: 8h 32m Mem: 5444.3/32093.9 MiB (17.0%)
Storage: 3.18 TiB (31.6% used) Procs: 365 Shell: Zsh inxi: 3.3.13
$ cargo +nightly --version --verbose
cargo 1.62.0-nightly (dba5baf 2022-04-13)
release: 1.62.0-nightly
commit-hash: dba5baf4345858c591517b24801902a062c399f8
commit-date: 2022-04-13
host: x86_64-unknown-linux-gnu
libgit2: 1.4.2 (sys:0.14.2 vendored)
libcurl: 7.80.0-DEV (sys:0.4.51+curl-7.80.0 vendored ssl:OpenSSL/1.1.1m)
os: Linux 2.8 [64-bit]
$ cargo +nightly bench
   Compiling xdigits2-bench v0.0.20220420 (/home/lo48576/src/dev/kotesaki-optimizations/xdigits2)
    Finished bench [optimized] target(s) in 0.56s
     Running unittests src/lib.rs (target/release/deps/xdigits2_bench-b84dfe3e71c5170e)

running 2 tests
test tests::micro_optimized ... bench:       2,284 ns/iter (+/- 18)
test tests::simple          ... bench:       2,336 ns/iter (+/- 63)

test result: ok. 0 passed; 0 failed; 0 ignored; 2 measured; 0 filtered out; finished in 1.79s

$ cargo +nightly bench
    Finished bench [optimized] target(s) in 0.00s
     Running unittests src/lib.rs (target/release/deps/xdigits2_bench-b84dfe3e71c5170e)

running 2 tests
test tests::micro_optimized ... bench:       2,259 ns/iter (+/- 7)
test tests::simple          ... bench:       2,322 ns/iter (+/- 53)

test result: ok. 0 passed; 0 failed; 0 ignored; 2 measured; 0 filtered out; finished in 0.56s

$ cargo +nightly bench
    Finished bench [optimized] target(s) in 0.00s
     Running unittests src/lib.rs (target/release/deps/xdigits2_bench-b84dfe3e71c5170e)

running 2 tests
test tests::micro_optimized ... bench:       2,260 ns/iter (+/- 7)
test tests::simple          ... bench:       2,325 ns/iter (+/- 33)

test result: ok. 0 passed; 0 failed; 0 ignored; 2 measured; 0 filtered out; finished in 0.62s

$
```
